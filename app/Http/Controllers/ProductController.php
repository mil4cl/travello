<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProjectResource;
use App\Models\Product;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Client;
use Illuminate\Http\Request;


class ProductController extends Controller
{
    public function index(Request $request)
    {

        $query = Product::query();

        if($request->has('start_date') && $request->has('ens_date') ){
            $query->whereHas('availability' ,function ($query) use ($request){
               $query->where('start_date', '>=', $request->get('start_date'))
                   ->where('end_date', '<=', $request->get('end_date'));
            });
        }
        $query->whereHas('availability' ,function ($query) use ($request){
            $query->where('start_date', '>=',now()->subDays(14))
                ->where('end_date', '<=', now());

;        });

        $products =  $query->with('availability:id,product_id,price')->get();


        $tours = $this->getAllTours();

        $prices = $this->getTourPrices();

        $toursInfo = $this->getAsyncTourInfo($tours);




        $result = [];

        foreach ($tours as $key => $tour)
        {
            $result[$key] = [
                'title' => $tour,
                'price' => $prices[$key],
            ];

        }

        foreach ($toursInfo as $tour){
            foreach ($result as  $key => $item){
                ($key === $tour['id']) ? $result[$key]['thumbnail'] = $tour['photos'][0]['url'] : null;
            }
        }

        $finalTours = collect();
        foreach ($result as $item) {
            $finalTours->push([
               'title' =>  $item['title'],
                'minimumPrice' => $item['price'],
               'thumbnail' => $item['thumbnail'] ?? null,
            ]);
        }



        return ProjectResource::collection($products)->concat(
             $finalTours
        );
      //  return response()->json($data);





    }

    /**
     * @return array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getAllTours()
    {

        $client = new Client();

        $tours = null;

        $response = $client->get(
            'https://1691846d-68ba-43ec-b38c-8f407a5b45a4.mock.pstmn.io/api/tours'
        );

        if ($response->getStatusCode() == \Symfony\Component\HttpFoundation\Response::HTTP_OK) {

            $tours = collect(json_decode($response->getBody()->getContents(), true))->pluck('title', 'id')->toArray();

        }

        return $tours;
    }

    /**
     * @return array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getTourPrices()
    {

        $client = new Client();
        $prices = null;
        $response = $client->get('https://1691846d-68ba-43ec-b38c-8f407a5b45a4.mock.pstmn.io/api/tour-prices');

        if ($response->getStatusCode() == \Symfony\Component\HttpFoundation\Response::HTTP_OK) {
            $prices = collect(json_decode($response->getBody()->getContents(),true))->pluck('price', 'tourId')->toArray();
        }

        return $prices;

    }

    private function getAsyncTourInfo($tours)
    {

        $responses = [];
        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://1691846d-68ba-43ec-b38c-8f407a5b45a4.mock.pstmn.io'
        ]);

        $requests = function () use ($client, $tours) {
            foreach($tours as $tourId => $tour) {
                yield function() use ($client,$tourId) {
                    return $client->getAsync("/api/tours/{$tourId}" );
                };
            }

        };


        $pool = new \GuzzleHttp\Pool($client, $requests(),[
            'concurrency' => 3,
            'fulfilled' => function (Response $response, $index) use (&$responses) {
                if ($response->getStatusCode() == 200) {
                    $responses[] = json_decode($response->getBody(), true);
                }

            },
            'rejected' => function (\GuzzleHttp\Exception\RequestException $reason, $index) {
                dd($reason);
            },
        ]);

        $pool->promise()->wait();

        return $responses;

    }

}
