<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class AvailabilityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id' => $this->faker->randomElement(Product::all('id')->pluck('id')->toArray()),
            'price' => '250.0',
            'start_date' => now(),
            'end_date' => $this->faker->dateTimeBetween('now' , '+4 week'),
        ];
    }
}
